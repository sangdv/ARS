import numpy as np
import os
import cv2
import shutil

SRC_PATH = './holes/'
DST_PATH = './processed_data/'
TRAINING_PERCENT = 0.8

def make_one_hot(label):
    res = np.zeros(2)
    res[label] = 1.0
    return res

print('Create directories......................................')
dirPaths = []
dirPaths.append(os.path.join(DST_PATH,'train'))
dirPaths.append(os.path.join(DST_PATH,'test'))
dirPaths.append(os.path.join(DST_PATH,'train/OK'))
dirPaths.append(os.path.join(DST_PATH,'train/NG'))
dirPaths.append(os.path.join(DST_PATH,'test/OK'))
dirPaths.append(os.path.join(DST_PATH,'test/NG'))
for i in range(len(dirPaths)):
    print(dirPaths[i])
    if os.path.exists(dirPaths[i]):
        shutil.rmtree(dirPaths[i])
    os.makedirs(dirPaths[i])

print('Reading and splitting data.................................')
data_ok = []
data_ng = []
all_data = []

ok_path = os.path.join(SRC_PATH, 'OK')
ng_path = os.path.join(SRC_PATH, 'NG')

for file in os.listdir(ng_path):
    img = cv2.imread(os.path.join(ng_path, file))
    img = cv2.resize(img, (48, 48))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = np.reshape(img, (48, 48, 1)) / 255.0
    label = make_one_hot(0)
    data_ng.append((img, label, os.path.join(ng_path, file)))
    all_data.append((img, label, os.path.join(ng_path, file)))

for file in os.listdir(ok_path):
    img = cv2.imread(os.path.join(ok_path, file))
    img = cv2.resize(img, (48, 48))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = np.reshape(img, (48, 48, 1)) / 255.0
    label = make_one_hot(1)
    data_ok.append((img, label, os.path.join(ok_path, file)))
    all_data.append((img, label, os.path.join(ok_path, file)))

np.random.shuffle(all_data)
np.random.shuffle(data_ok)
np.random.shuffle(data_ng)
num_samples = len(all_data)
print(num_samples)

#train_data = np.append(data_ok[:(int)(TRAINING_PERCENT * len(data_ok))], data_ng[:(int)(TRAINING_PERCENT * len(data_ng))], axis = 0)
#test_data = np.append(data_ok[(int)(TRAINING_PERCENT * len(data_ok)):], data_ng[(int)(TRAINING_PERCENT * len(data_ng)):], axis = 0)

train_data = np.append(data_ok[:9600], data_ng[:800], axis = 0)
test_data = np.append(data_ok[9600:], data_ng[800:], axis = 0)
#print(np.shape(train_data))
for i in range(len(data_ok)):
    path = data_ok[i][2]
    img = cv2.imread(path)
    path = path.split("/")
    path = path[len(path)-1]    
    if i < 9600:
        cv2.imwrite(os.path.join(DST_PATH, 'train/OK/', path), img);
    else:
        cv2.imwrite(os.path.join(DST_PATH, 'test/OK/', path), img);

for i in range(len(data_ng)):
    path = data_ng[i][2]
    img = cv2.imread(path)
    path = path.split("/")
    path = path[len(path)-1]    
    if i < 800:
        cv2.imwrite(os.path.join(DST_PATH, 'train/NG/', path), img);
    else:
        cv2.imwrite(os.path.join(DST_PATH, 'test/NG/', path), img);


print(np.shape(train_data))
print(np.shape(test_data))

np.save(os.path.join(DST_PATH, 'train_data.npy'), train_data)
np.save(os.path.join(DST_PATH, 'test_data.npy'), test_data)

num_ok = 0
num_ng = 0
num_test_ok = 0
num_test_ng = 0
for i in range(len(train_data)):
	if train_data[i][1][0] == 1.0:
		num_ng += 1
	else:
		num_ok += 1

for i in range(len(test_data)):
    if test_data[i][1][0] == 1.0:
        num_test_ok += 1
    else:
        num_test_ng += 1

print(num_ng)
print(num_ok)
print(num_test_ng)
print(num_test_ok)
#cv2.imshow('img', data_ng[0][0])
#cv2.waitKey(0)
#print(data_ok[0][0])
#print(np.linalg.norm(np.reshape(data_ng[0][0], (48 * 48)),0))
#print(np.linalg.norm(np.reshape(data_ok[0][0], (48 * 48)),0))
print('Done !')


