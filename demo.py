from Tkinter import Tk
from tkFileDialog import askopenfilename
import tkMessageBox as messagebox
import DenseNet
import DenseNet_input
import cv2
import sys
import numpy as np

train, test = DenseNet_input.getEmotionImage()
model = DenseNet.DenseNet(train, test, test_data=test, input_size=42, num_class=2, init_lnr=1e-1, depth=3 * 3 + 4,
                      bc_mode=True, reduction=0.5, weight_decay=1e-4, total_blocks=3, lnr_update_center=0.5,
                      weight_center_loss=0.5, growth_rate=12, reduce_lnr=[15, 30, 45],
                      current_save_folder='./save/DenseNet9600/', logs_folder='./summary/DenseNet9600',
                      valid_save_folder='./save/DenseNet9600/valid/',
                      max_to_keep=0, snapshot_test=False)

Tk().withdraw() 
filename = askopenfilename() 
img = cv2.resize(cv2.imread(filename, 0) / 255.0, (42, 42))[np.newaxis, :, :, np.newaxis]
messagebox.showinfo(title="Result", message=model.test_a_hole('./save/DenseNet9600/valid/model1.0.ckpt',img))