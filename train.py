from __future__ import print_function
import DenseNet
import DenseNet_input
import numpy as np

train, test = DenseNet_input.getEmotionImage()

model = DenseNet.DenseNet(train, test, test_data=test, input_size=42, num_class=2, init_lnr=1e-2, depth=3 * 3 + 4,
                          bc_mode=True, reduction=0.5, weight_decay=1e-4, total_blocks=3, lnr_update_center=0.5,
                          weight_center_loss=0, growth_rate=12, reduce_lnr=[50, 100, 150],
                          current_save_folder='./save/DenseNet9600/', logs_folder='./summary/DenseNet9600',
                          valid_save_folder='./save/DenseNet9600/valid/',
                          max_to_keep=0, snapshot_test=False)
model.train(num_epoch=100, batch_size=100)

# _, mean_acc, all_score, all_label, all_path = model.test(batch_size=256, save_file='model2.ckpt')
# num_samples = len(all_label)
# wrong = []
# for i in range(num_samples):
#     if np.argmax(all_score[i]) != np.argmax(all_label[i]):
#         wrong.append(all_path[i])
# print(num_samples)
# print(len(wrong))
# print(wrong)
