# Implement densely connected convolutional network with TensorFlow framework

## 1. Installation

### 1.1. Install tensorflow in Anaconda
https://www.tensorflow.org/install/install_linux#InstallingAnaconda

Cái này đã cài ở AI server ARS. 

Dùng lệnh sau để vào môi trường tensorflow:
source activate tensorflowbk

Nếu cài mới chỉ cài phiên bản cho python 2.7, ko dùng python 3.x.

### 1.2. Install packages
- Activate tensorflow in anaconda: source activate tensorflowbk
- Install tkinker: conda install -c anaconda tk
- Install opencv (if needed): conda install -c conda-forge opencv

### 1.3. Clone source code
git clone https://gitlab.com/sangdv/ARS.git

## 2. Read and split data
python read_data.py

- SRC_PATH: directory contains raw images
- DST_PATH: directory to save *.npy file

## 3. Training model
python train.py

- input_size: Size of the input image. If it smaller than original image, we use random crop in data augmentation phase
- num_class: Number of classes of our task
- init_lnr: The started learning rate when we train our model
- depth: It should be: total_blocks . x + total_blocks + 1
- bc_mode: Using bc mode or not
- reduction: Compression parameter, it should equal to 1.0 if bc_mode = False
- lnr_update_center: Learning rate for updating center if you wanna use center loss
- weight_center_loss: Weight of center loss 
- reduce_lnr: When you reduce the model's learning rate?

## 4. Demo
python demo.py
python FFNN_demo.py

You can change the model to be used for inference by changing the model path in the following line:
model.test_a_hole('./save/DenseNet9600/valid/model1.0.ckpt',img)