import numpy as np
import DenseNet
import DenseNet_input
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

train, test = DenseNet_input.getEmotionImage()

model = DenseNet.DenseNet(train, test, test_data=test, input_size=42, num_class=2, init_lnr=1e-1, depth=3 * 3 + 4,
                          bc_mode=True, reduction=0.5, weight_decay=1e-4, total_blocks=3, lnr_update_center=0.5,
                          weight_center_loss=0.5, growth_rate=12, reduce_lnr=[15, 15, 15],
                          current_save_folder='./save/DenseNet1/', logs_folder='./summary/DenseNet1',
                          valid_save_folder='./save/DenseNet9600/valid/',
                          max_to_keep=0, snapshot_test=False)
score, label = model.test_snapshot_ensemble(batch_size=1, save_file=[1.0])


def PlotConfusionMatrix(conf_arr):
    norm_conf = []
    for i in conf_arr:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    plt.clf()
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.jet,
                    interpolation='nearest')

    width, height = conf_arr.shape

    for x in range(width):
        for y in range(height):
            ax.annotate(str(round(norm_conf[x][y], 5)), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center')

    cb = fig.colorbar(res)
    alphabet = '0123456789JKLMNOPQRSTUVWXYZ'
    plt.xticks(range(width), alphabet[:width])
    plt.yticks(range(height), alphabet[:height])
    plt.savefig('confusion_matrix.png', format='png')
    plt.show()


y_pred = []
y_true = []
wrong = np.zeros([7])
for i in range(len(score)):
    true_label = np.argmax(label[i])
    pred = np.argmax(score[i])
    y_true.append(true_label)
    y_pred.append(pred)

conf_matrix = confusion_matrix(y_true, y_pred)
PlotConfusionMatrix(conf_matrix)
